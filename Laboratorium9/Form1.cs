﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratorium9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lv.View = View.Details;
            lv.GridLines = true;
            lv.FullRowSelect = true;

            lv.Columns.Add("Login", 100);
            lv.Columns.Add("Password", 300);
            lv.Columns.Add("Service", 200);

            StreamReader r = new StreamReader("database.json");
            string json = r.ReadToEnd();
            var app = JsonConvert.DeserializeObject<App>(json);
            r.Close();
            r.Dispose();

            var list = app.AccountList;

            foreach (var item in list)
            {
                lv.Items.Add(Tools.AddData(item));
            }

        }

        private void lv_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbLogin.Text)
                || string.IsNullOrWhiteSpace(tbPassword.Text)
                || string.IsNullOrWhiteSpace(tbService.Text))
            {
                return;
            }

            var account = new Account(tbLogin.Text, tbPassword.Text, tbService.Text);
            lv.Items.Add(Tools.AddData(account));


            File.WriteAllText("database.json", JsonConvert.SerializeObject(account));
            tbLogin.Clear();
            tbPassword.Clear();
            tbService.Clear();
        }

        private void btMoreInfo_Click(object sender, EventArgs e)
        {
            var indices = lv.SelectedItems.Count;
            if (indices < 1)
                return;

            MessageBox.Show($"Password: {lv.SelectedItems[0].SubItems[1].Text}");
        }

        private void btRemove_Click(object sender, EventArgs e)
        {
            var indices = lv.SelectedItems.Count;
            if (indices < 1)
                return;

            lv.SelectedItems[0].Remove();
            MessageBox.Show("Record removed");
        }
    }
}
