﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Laboratorium9
{
    class Tools
    {
        public static ListViewItem AddData(Account obj)
        { 
            string[] arr = new string[3];
            arr[0] = obj.Login;
            arr[1] = obj.Password;
            arr[2] = obj.Service;
            var item = new ListViewItem(arr);
            return item;
        }
    }
}
