﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorium9
{
    class Account
    {
        public Account(string login, string password, string service)
        {
            Login = login;
            Password = password;
            Service = service;
        }
    
        public string Login { get; set; }
        public string Password { get; set; }
        public string Service { get; set; }
    }
}
